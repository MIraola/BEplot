import numpy as np


def GenPath(kvecs, rvec, borders, indHSP):
    '''
    Generate path to use a X-axis values in plot
    + Inputs:
        - kvecs: array; fractional coordinates of k-points (w/o repetitions)
        - rvec: array; each row contains cartesian coordinates of a rec vec
        - borders: indices of bottom of continous segments
        - indHSP:  array; indices of hsp in kvec (w/o repetitions)
    + Output:
        - X: array; values for X-axis in the plot
        - Xgrid: list; 
    '''

    X= np.zeros(kvecs.shape[0], dtype=float)
    Xgrid= [] #localtions of HSPs in plot
    X[0]= 0. #X-axis values in plot
    for ib,it in zip(borders[:-1],borders[1:]):
        kloc= kvecs[ib:it] #continous segment in path
        X[ib]= X[ib-1] #x-value occupied with energies of 2 HSPs
        X[ib+1:it]= np.cumsum(np.linalg.norm(np.einsum('ik,kj->ij',kloc[1:]-kloc[:-1],rvec), axis= 1))+ X[ib]
        Xgrid.append(X[ib])
    Xgrid+= [X[ind] for ind in indHSP if ind not in borders and ind+1 not in borders] + [X[-1]]
    return X, sorted(Xgrid, key= float)

def GenPathMBJ(hsp, Nk):
    "Generates path of K-points to write in KPOINTS for MBJ, also in multisegment case (TEST IT!!)"

    lengths= [Kseg.shape[0]-1 for Kseg in hsp]
    Kvecs= np.zeros((sum(lengths)* Nk, 3))
    for iseg, Kseg in enumerate(hsp): #continous segment in path
        for i, (Kbot, Ktop) in enumerate(zip(Kseg[:-1], Kseg[1:])):
            Kvecs[Nk* (sum(lengths[:iseg])+ i) : Nk* (sum(lengths[:iseg])+ i+ 1),:]= np.linspace(Kbot, Ktop, num= Nk, endpoint= True)
    return Kvecs

def KPTwrite(IBZKPT,Kpath,opt):
    '''Writes the KPOINT file for a self-consistent bandstructure MBJ calculation'''
    
    fIBZ=open(IBZKPT,'r')
    f=open("KPOINTS",'w')
    
    f.write('K-points for self-consistent MBJ bands calculation. Generated with VaspPlot.\n')
    fIBZ.readline()
    NIBZ=int(fIBZ.readline())
    assert opt.lower()==fIBZ.readline()[0].lower(), "Different coordinate choice in IBZKPT and KPOINTS_BS"
    f.write("{}\n".format(NIBZ+Kpath.shape[0]))
    f.write('Reciprocal\n' if opt.lower()=='r' else 'Cartesian\n')
    for k in Kpath:
        f.write("{0:12.9f}   {1:12.9f}   {2:12.9f}   0\n".format(k[0],k[1],k[2]))
    for line in fIBZ:
        f.write(line)
    
    fIBZ.close()
    f.close()


def printKPT(fEIG="EIGENVAL"):
    
    f=open(fEIG,'r')
    for i in range(6):
        line=f.readline()
    f.readline()
    NK,NB=np.array(line.split()[1:],dtype=int)
    for ik in range(NK):
        K = np.array(f.readline().split()[:-1],dtype=float)
        K="{0:6.3f}   {1:6.3f}   {2:6.3f}    index:{3:4d}".format(K[0],K[1],K[2],ik+1)
        print(K)
        for ib in range(NB+1):
            f.readline() #energies
    f.close()


def IdentifyJumps(Kvecs):
    '''Detects jumps in the path'''

    return np.where(np.linalg.norm(Kvecs[1:]-Kvecs[:-1], axis= 1))[0]+1

