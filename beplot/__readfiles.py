
import numpy as np
bhor2ang=0.5292

class ReadVasp:

    def __init__(self, OUTCAR, KPOINTS, EIGENVAL, mbj):
        print("Reading info from VASP output files: \n OUTCAR: {} \n KPOINTS: {} \n EIGENVAL: {}".format(OUTCAR, KPOINTS, EIGENVAL))
        self.mbj = mbj
        self.KPT= KPOINTS
        self.OUT= OUTCAR
        self.EIG= EIGENVAL

    def ReadKPOINTS(self):
        '''Read high-symmetry points (in outcar are wrong)
        + Input: fKPT = name of KPOINTS file
        + Output:
            - kpnames: list; as many elements as sections in total path. Each element is a list with labels of HSP.
            - hsp: array with coordinates of HSP.
        '''
        
        fKPT= open(self.KPT, "r")
        hsp = []
        
        # create labels for the x-axis in the plot
        line = fKPT.readline().split()
        for i,path in enumerate(line):
            kloc= [s if s!="GM" else r"$\Gamma$" for s in path.split('-')]
            # while "GM" in kloc: kloc[kloc.index("GM")]=r"$\Gamma$" #substitute GM by latex \Gamma
            if i==0: 
                kpnames= kloc
            else: 
                kpnames[-1]+= "|"+kloc[0]
                kpnames+= kloc[1:]            
                
        Nk= int(fKPT.readline()) #kpts between 2 hsp
        for i in range(2): line= fKPT.readline()
        assert line.split()[0].lower()!='r', 'KPOINTS in cartesian basis not implemented'
        
        #read coor of hsp in path
        for line in fKPT:
            K= list(map(float,line.split()[:3]))
            if K not in hsp and len(K)==3: hsp.append(K) #avoid repetition & blank lines
        # nkpts= None
        # if self.mbj: nkpts= (len(kpnames)-1)*Nk
        
        fKPT.close()
        return kpnames, np.array(hsp), Nk

    def ReadOUTCAR(self, EF):

        fOUT= open(self.OUT, "r")

        rvec= None
        for line in fOUT:
            if 'NKPTS' in line:
                nkpts = int(line.split()[3]) #substituted later if mbj
                nbands = int(line.split()[14])
            if "NELECT" in line:
                Nele = int(float(line.split()[2]))
            if "IALGO" in line:
                if int(line.split()[2])== 58: print("Electronic optimization algorithm found: CGA. EF may be unreliable !")
            if "LSORBIT" in line:
                soc = False if line.split()[2]=="F" else True
            if "direct lattice vectors" in line and "reciprocal lattice vectors" in line and rvec is None: #read recip vectors
                rvec= np.zeros((3,3), dtype= float)
                for i in range(3):
                    rvec[i]= np.array(fOUT.readline().split()[3:6], dtype= float)
            if EF is None: #look for EF
                if "E-fermi" in line: EF= float(line.split()[2])

        fOUT.close()
        return nkpts, nbands, Nele, soc, rvec, EF

    def ReadEIGENVAL(self, nkpts, kpnames, hsp, ik0=None):
        '''Reads EIGENVAL.
        + Input: fEIG = name of EIGENVAL file
        + Output: 
            - Ekn (Nk_total,IBend-IBstart+1): element [ik,n] is the energy of n^th band in K-point ik. 
            - Kvecs = list; as indep elements as independent paths in the whole path; each element is an array with the direct cood. of a k-point.
            - indHSP = array; indices of hsp in kvecs'''

        fEIG= open(self.EIG, "r")

        #read nbands
        for i in range(6): line= fEIG.readline()
        self.nbands= int(line.split()[2])
        fEIG.readline()

        # if kpoints of scf are first -> jump them
        if self.mbj:
            for ik in range(ik0):
                K, E = self.__ReadBlock(fEIG)

        Kold = np.array([10.,10.,10.]) #initialize
        Nrep= len([s for s in kpnames if "|" not in s])-2 #numb of repeated kpoints in EIGENVAL
        kvecs, Ekn= np.zeros((nkpts-Nrep,3), dtype= float), np.zeros((nkpts-Nrep,self.nbands), dtype= float) 
        ind_kvecs= 0 #hsp may be repeated in eigenval
        borders= [] #index in kvecs where path has a jump
        for ik in range(nkpts): 
            K, E= self.__ReadBlock(fEIG)
            # print(ik,K)
            if np.any(np.all(np.isclose(hsp,K, atol= 1e-5, rtol= 0.),axis= 1)):#it is a hsp
                if np.allclose(K,Kold,atol=1e-5,rtol=0.) == False: #if not repeated->jump
                    kvecs[ind_kvecs], Ekn[ind_kvecs]= K, E
                    borders.append(ind_kvecs)
                    ind_kvecs+= 1
            else: #not hsp
                kvecs[ind_kvecs], Ekn[ind_kvecs]= K, E
                ind_kvecs+= 1
            Kold = K
        fEIG.close()
        return Ekn, kvecs, borders

    def __ReadBlock(self, fEIG):
        '''Read a block of a K-point in EIGENVAL'''

        K= np.round(np.array(fEIG.readline().split()[:3],dtype=float),6)
        E= np.zeros(self.nbands, dtype= float) #init
        for n in range(self.nbands):
            # line= fEIG.readline()
            # print(line)
            E[n]= float(fEIG.readline().split()[1])
            # E[n]= float(line.split()[1])
        fEIG.readline() #blank line
        return K, E

    def ReadKPT4MBJ(self):
        '''Read high-symmetry points for generation of KPOINTS for MBJ calculation
        + Input: fKPT = name of KPOINTS file that would work for LDA/GGA
        + Output:
            - kpnames: list; as many elements as sections in total path. Each element is a list with labels of HSP.
            - K: list; len=num. of paths; each element is an array with hsp in rows.
            - N: num. of kpts between two hsp. 
            - opt: str.; whether reciprocal or cartesian coordinates are used.
        '''

        fKPT= open(self.KPT, "r")

        K = []; kpnames= []
        line = fKPT.readline().split()
        
        for path in line:
            kpnames.append([Kstr for Kstr in path.split('-')])
        N= int(fKPT.readline()) #numb. kpt between hsps
        for i in range(2): line=fKPT.readline()
        opt= line.split()[0][0] #direct/cart. coord.
        for i in range(len(kpnames)):
            Kloc= []; Kold= np.linspace(2.,3.,num=3)
            for j in range(len(kpnames[i])-1):
                if i!= 0 or j!= 0:
                    fKPT.readline()
                for ik in range(2):
                    k= np.array([ki for ki in fKPT.readline().split()[:3]], dtype= float)
                    if np.allclose(Kold, k, rtol= 0.)== False:
                        Kloc.append(k)
                    Kold= k
            K.append(np.array(Kloc))
        fKPT.close()
        return K,N,opt


class ReadAbinit:

    def __init__(self, file, hsp):
        print("Reading information from ABINIT file: ", file)
        self.file= open(file, "r")
        self.hsp= hsp

    def ReadOUT(self):
        
        jdtset= None #identify dtset with band calculation
        while jdtset is None:
            line= self.file.readline().split()
            if "ndtset" in line:
                ndtset= int(line[1])
                if ndtset> 1: jdtset=int(input("Found {:2d} datasets. Give the number of the dataset containing the calculation of bands:".format(ndtset)))
        self.file.seek(0) #go to beginning to read spinor

        #find and read nspinor
        spinor= None
        while spinor is None:
            if self.file.readline().split()[:2]== ["DATASET", str(jdtset)]: 
                for i in range(5): line= self.file.readline()
                spinor= True if int(line.split()[11])== 2 else False
        
        #find and read nbands
        self.nbands= None
        while self.nbands is None:
            line= self.file.readline().split()
            try: l0, l1= line[0], int(line[1])
            except: continue
            if l0== "nband"+ (str(jdtset) if ndtset> 1 else ""): self.nbands= l1 

        #find and read nkpt in path
        nkpts= None
        while nkpts is None:
            line= self.file.readline().split()
            if "nkpt"+ (str(jdtset) if ndtset> 1 else "") in line: nkpts= int(line[1])

        #skip lines until dataset of bands
        if ndtset> 1:
            boolean= True
            while boolean: 
                line= self.file.readline().split()
                try: a, b= line[1], line[2]
                except: continue
                if line[1]== "DATASET" and int(line[2])== jdtset: boolean= False

        #read reciprocal basis vectors
        rvec= np.zeros((3,3))
        boolean= True
        while boolean: 
            line= self.file.readline().split("+")
            if line[0]== " Real(R)" and line[1].split()[0]== "Recip(G)":
                for i in range(3): rvec[i,:]= np.array(self.file.readline().split()[5:], dtype=float)/bhor2ang
                boolean= False

        #read k-points & energy-levels
        Kvecs, Ekn= np.zeros((nkpts,3)), np.zeros((nkpts, self.nbands))
        boolean= True
        while boolean:
            if "Eigenvalues (   eV  )" in self.file.readline(): boolean= False
        ind_hsp=[]
        for ik in range(nkpts):
            Kvecs[ik], Ekn[ik]= self.ReadBlock_abinit()
            # for n in range(Ekn.shape[1]): print(n, Ekn[0,n])
            # exit()
            if np.any(np.all(np.isclose(self.hsp,Kvecs[ik], atol= 1e-5, rtol= 0.),axis= 1)):#it is a hsp
              ind_hsp.append(ik)  
        
        self.file.close()        
        return {"spinor": spinor, "nbands": self.nbands, "nkpts": nkpts, "rvec": rvec,
                "Kvecs": Kvecs, "Ekn": Ekn, "ind_hsp": ind_hsp} 

    def ReadBlock_abinit(self):
        
        K= np.array(self.file.readline().split(",")[3].strip().split()[1:4], dtype= float)
        En= np.zeros((self.nbands), dtype= float)
        for i in range(int(self.nbands/8)+ (1 if self.nbands%8!= 0 else 0)): 
            if i<= int(self.nbands/8): En[8*i:8*(i+1)]= np.array(self.file.readline().split(), dtype= float)
            else: En[8*i:]= np.array(self.file.readline().split(), dtype= float)
        return K, En

class ReadWannier():
    def __init__(self, seedname):
        self.seedname = seedname
    
    def read_bands_dat(self):
        '''
        Read Ekn from _band.dat file
        '''
        with open(self.seedname+'_band.dat') as file:
            # todo: improve efficiency
            X = []
            flag = True
            E_kn = []
            E_k = []
            for line in file:
                if len(line.split())==0:
                    E_kn.append(E_k)
                    E_k = []
                    flag = False
                else:
                    E_k.append(line.split()[1])
                    if flag:
                        X.append(line.split()[0])
        return np.array(E_kn, dtype=float).T, np.array(X, dtype=float)/(2.0*np.pi)
                    
