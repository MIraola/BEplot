import numpy as np
import matplotlib.pyplot as plt
from .__tools2plot import BandStr,str2,Plotting
from .__readfiles import ReadVasp, ReadWannier
import sys
from .__toolsKPT import *


#HELP MESSAGE

def hlp():
    print ( """   Plots bands obtained with VASP in a band structure calculation (ICHARG = 11) 

   usage: 
       python -m beplot IBstart=... IBend=... fKPT=... fOUT=... fEIG=...

    -h   print this help message

    NOTE: the following files should be in the working directory 
          from which the script is run

          - OUTCAR
          - KPOINTS (band structure calculation)
          - EIGENVAL

    IBstart  index of the first band that should be plotted (look in EIGENVAL)
    IBend  index of the last band that should be plotted (look in EIGENVAL)
    fKPT  input KPOINTS file used for a bandstructure calc. in VASP. default: KPOINTS
    fOUTCAR  input OUTCAR file written by VASP.  default: OUTCAR
    fEIG  input EIGENVAL file written by VASP. default: EIGENVAL
    EF fermi-energy as written in the OUTCAR of the scf calculation
       
""")

# FUNCTION TO READ KPNAMES

def ReadKpnames(v):
    kloc= [[ kname if kname!="GM" else r"$\Gamma$" for kname in path.split("-")] for path in v.split(",,")]
    for i,s in enumerate(kloc):
        if i==0: kpnames= s
        else:
            kpnames[-1]+= "|"+ s[0]
            kpnames+= s[1:]
    return kpnames


# DEFAULT VALUES

inputs={"vasp":{},"espresso":{},"abinit":{},"wannier":{},"general":{}}

inputs["general"]["IBstart"] = None
inputs["general"]["IBend"] = None
inputs["vasp"]["KPOINTS"] = 'KPOINTS'
inputs["vasp"]["OUTCAR"] = 'OUTCAR'
inputs["vasp"]["EIGENVAL"] = 'EIGENVAL'
inputs["espresso"]["fIN"] = "bands.in"
inputs["espresso"]["fOUT"] = "bands.out"
inputs["general"]["EF"] = None
inputs["general"]["ybot"] = None
inputs["general"]["ytop"] = None
inputs["vasp"]["mbj"] = False
inputs["vasp"]["fKPT_MBJ"] = "KPOINTS_MBJ_BS"
inputs["vasp"]["genMBJpath"] = False
inputs["vasp"]["IBZKPT"] = 'IBZKPT'
inputs["general"]["printK"] = False
inputs["general"]["code"] = "vasp"
inputs["espresso"]["kpnames"] = None
inputs["espresso"]["spinor"] = None
inputs["abinit"]["fOUT"] = None
inputs["general"]["hsp"] = None
inputs["general"]["kpnames"] = None
inputs["general"]["Nele"] = 100000
inputs["general"]["Nk"] = 100 #for genMBJpath
inputs["general"]["title"] = None
inputs["general"]["color"] = None
inputs["general"]["fsave"] = None
inputs["general"]["compare"] = False
inputs["wannier"]["seedname"] = 'wannier90'


# IDENTIFY COMMAND-LINE INPUTS

for arg in sys.argv[1:]:
    if arg=="-h": 
        hlp()
        exit()
    k=arg.split("=")[0]
    v="=".join(arg.split("=")[1:])
    if k=='IBstart': inputs["general"]["IBstart"]=int(v)
    elif k=='IBend': inputs["general"]["IBend"]=int(v)
    elif k=='fOUT': inputs["vasp"]["OUTCAR"]=v
    elif k=='fEIG': inputs["vasp"]["EIGENVAL"]=v
    elif k=='fKPT': inputs["vasp"]["KPOINTS"]=v
    elif k=='EF': inputs["general"]["EF"]=float(v)
    elif k=='ybot': inputs["general"]["ybot"]=float(v)
    elif k=='ytop': inputs["general"]["ytop"]=float(v)
    # elif k=='gap': gap=str2().bool(v)
    # elif k=='gap_K': gap_K=v.split(',')
    # elif k=='IBlast': IBlast=int(v)
    elif k=='mbj': inputs["vasp"]["mbj"]=True
    elif k=='fKPT_MBJ': inputs["vasp"]["fKPT_MBJ"]=v
    elif k=='genMBJpath': inputs["vasp"]["genMBJpath"]=True
    elif k=='IBZKPT': inputs["vasp"]["IBZKPT"]=v
    elif k=='printK': inputs["general"]["printK"]=True
    elif k=='code': inputs["general"]["code"]=v.lower()
    elif k=='spinor': inputs["espresso"]["spinor"]=str2(v).bool()
    elif k=='fIN_qe': inputs["espresso"]["fIN"]=v
    elif k=='fOUT_qe': inputs["espresso"]["fOUT"]=v
    elif k=='fOUT_abinit': inputs["abinit"]["fOUT"]=v
    elif k=='hsp': inputs["general"]["hsp"]= np.array([ ki.split(",") for ki in v.split(",,")], dtype= float)
    elif k=='kpnames': inputs["general"]["kpnames"]= ReadKpnames(v)
    elif k=='Nele': inputs["general"]["Nele"]= int(v) #for abinit
    elif k=='Nk': inputs["general"]["Nk"]= int(v)
    elif k=='title': inputs["general"]["title"] = v
    elif k=='color': inputs["general"]["color"] = v
    elif k=='fsave': inputs["general"]["fsave"] = v
    elif k=='compare': inputs["general"]["compare"] = str2(v).bool()
    elif k=='file_dat': inputs["wannier"]["seedname"] = v


# RUNNING MAIN PROGRAM

if inputs["vasp"]["genMBJpath"]:
    hsp, Nk, opt= ReadVasp(inputs["vasp"]["OUTCAR"], inputs["vasp"]["KPOINTS"], inputs["vasp"]["EIGENVAL"], inputs["vasp"]["mbj"]).ReadKPT4MBJ()
    Kvecs= GenPathMBJ(hsp, Nk)
    KPTwrite(inputs["vasp"]["IBZKPT"],Kvecs,opt)
    exit()

elif inputs["general"]["printK"]: # implement for QE!!
    print("Printing K-points and their indices (starting from 1)")
    printKPT(inputs["vasp"]["EIGENVAL"])
    exit()

BS=BandStr(inputs)
print('Found {nkpts} K-points and {nbands} bands'.format(nkpts=BS.nkpts,nbands=BS.nbands))
print('Bands from {ibstart} to {ibend} will be plotted'.format(ibstart=BS.IBstart,ibend=BS.IBend))

# compare to W90 bands
if inputs["general"]["compare"]:
    Ekn_wannier, X_wannier = ReadWannier(inputs["wannier"]["seedname"]).read_bands_dat()
    Plotting(BS).compare_wannier(Ekn_wannier, 
                                 X_wannier, 
                                 BS.ef,
                                 inputs["general"]["color"],
                                 inputs["general"]["ybot"],
                                 inputs["general"]["ytop"],
                                 inputs["general"]["title"],
                                 inputs["general"]["fsave"]
                                 )
    exit()

Plotting(BS).plotbands(inputs["general"]["color"],
                       inputs["general"]["ybot"],
                       inputs["general"]["ytop"],
                       inputs["general"]["title"],
                       inputs["general"]["fsave"]
                       )

# if gap: #TO BE FIXED
    
#     assert IBlast is not None, 'Give the number of electrons in the unit cell (IBlast)'
#     BS.SearchGap(gap_K,IBlast)
