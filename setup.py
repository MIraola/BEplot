import setuptools

with open("README", "r") as fh:
    long_description = fh.read()


from beplot.__version import __version__ as version

setuptools.setup(
     name='BEplot',  
     version=version,
     author="Mikel Iraola Inurrieta",
     author_email="mikeliraola95@gmail.com",
     description="a tool to plot band structures calculated with some DFT codes",
     long_description=long_description,
     long_description_content_type="text/markdown",
     install_requires=['numpy', 'matplotlib'],
    #  include_package_data=True,
     url="https://gitlab.com/MIraola/BEplot",
     packages=setuptools.find_packages(),
     classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT license",
         "Operating System :: OS Independent",
     ],
 )