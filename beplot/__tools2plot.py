import numpy as np
import matplotlib.pyplot as plt
from .__readfiles import *
from .__toolsKPT import *

class BandStr:

    def __init__(self,d):
        self.kpnames=d["general"]["kpnames"] #if vasp, it will be overwritten
        self.hsp= d["general"]["hsp"] #if vasp, it will be overwritten
        
        self.code=d["general"]["code"]
        if self.code == 'vasp': self.__init__vasp(d)
        elif self.code== 'espresso': self.__init__espresso(d["espresso"], EF=d["general"]["EF"])
        elif self.code== 'abinit': self.__init__abinit(d)
        
        #set IBstart/end
        self.IBstart= d["general"]["IBstart"] if d["general"]["IBstart"] is not None else 0
        self.IBend= d["general"]["IBend"] if d["general"]["IBend"] is not None else self.nbands
        self.Ekn= self.Ekn[:,self.IBstart:self.IBend]

        #identify top of continous segments
        self.borders= self.__identify_borders()
        #energy offset= EF
        if d["general"]["EF"] is not None: self.ef= d["general"]["EF"]
        self.Ekn-= self.ef 
        
        #calculate x-values and locations of HSP for the plot
        self.X, self.Xgrid= GenPath(self.Kvecs, self.rvec, self.borders, self.indHSP) #x-array and grids for certain indep path

    def __init__vasp(self,d):

        self.mbj=d["vasp"]["mbj"]
        # reak ik0 for MBJ (passed to ReadEIGENVAl)
        if self.mbj: ik0=self._KPTMBJ_first(d["vasp"]["fKPT_MBJ"])
        else: ik0=None
        RV= ReadVasp(d["vasp"]["OUTCAR"], d["vasp"]["KPOINTS"], d["vasp"]["EIGENVAL"], d["vasp"]["mbj"])
        #read OUTCAR
        self.nkpts, self.nbands, self.Nele, self.spinor, self.rvec, self.ef= RV.ReadOUTCAR(d["general"]["EF"])
        #read KPOINTS
        self.kpnames, self.hsp, Nk= RV.ReadKPOINTS()
        if self.mbj: self.nkpts= Nk*(len(self.kpnames)-1)  #in MBJ, dont count ibzkpt's points
        #read EIGENVAL
        self.Ekn, self.Kvecs, self.indHSP= RV.ReadEIGENVAL(self.nkpts, self.kpnames, self.hsp, ik0)
        

    def __init__abinit(self, d):

        self.Nele= d["general"]["Nele"] #not found in out 
        if d["general"]["EF"] is None: d["general"]["EF"]= 0. #in abinit EF not read (only written in scf datasets)
        data= ReadAbinit(d["abinit"]["fOUT"], self.hsp).ReadOUT()
        self.spinor, self.nbands, self.nkpts, self.rvec, self.Kvecs, self.Ekn, self.indHSP = \
                     data["spinor"], data["nbands"], data["nkpts"], data["rvec"], data["Kvecs"], data["Ekn"], data["ind_hsp"] 
        

    def __init__espresso(self,dqe,EF):
       
        self.Npaths=1 #not sure if QE can have discontinuous paths
        self.EF=EF
        self.spinor=dqe["spinor"] #not in .out of calculation=bands
        assert self.spinor is not None, "spinor is mandatory for Quantum Espresso"
        self.kpnames=[dqe["kpnames"]] #not in input/output files
        
        self.hsp=self.read_QE_hsp(dqe["fIN"])
        
        f=open(dqe["fOUT"],'r')
        for line in f:
            if "number of electrons" in line: 
                self.Nele=int(float(line.split()[4]))
                self.nbands=int(f.readline().split()[4])
                if self.IBend is None: self.IBend=self.nbands
            elif "celldm(1)" in line: alat=float(line.split()[1])*bhor2ang
            elif "reciprocal axes:" in line: self.rvec=np.array([np.array(f.readline().split()[3:6],dtype=float) for i in range(3)])*2.*np.pi/alat
            elif "number of k points" in line: 
                self.nkpts=int(line.split()[4])
                for i in range(self.nkpts+3): f.readline() #cartesian coord.
                self.Kvecs=[]
                for i in range(self.nkpts): #coord. respect to primitive vectors
                    self.Kvecs.append([ki[:9] for ki in f.readline().split()[4:7]]) #10th character is a )
                self.Kvecs=[np.array(self.Kvecs,dtype=float)]
            elif "End of band structure calculation" in line:
                f.readline()
                self.Ekn=[]
                for ik in range(self.nkpts): 
                    self.Ekn.append(self.read_QE_k(f))
                self.Ekn=np.array(self.Ekn)-(self.EF if self.EF is not None else 0.0)
                self.Ekn=self.Ekn[:,self.IBstart:self.IBend]
                f.close();break #break before reaching the EOF
                
    def __identify_borders(self):
        '''Return [1,tops of continous segments,lastK]'''
    
        borders=[]
        for i,s in enumerate(self.kpnames):
            if "|" in s: borders.append(self.indHSP[i+len(borders)+1])
        borders= [0]+ borders+ [self.Kvecs.shape[0]]
        return borders

    def read_QE_hsp(self,fIN):

        hsp=[]
        f=open(fIN,'r')
        for line in f:
            if "K_POINTS" in line:
                n=int(f.readline().split()[0])
                for i in range(n):
                    K=f.readline().split()[:3]
                    if K not in hsp: hsp.append(K)
                f.close()
                break
        return np.array(hsp,dtype=float)


    def read_QE_k(self,f):
        
        # k=np.array(f.readline().split()[2:5],dtype=float)
        for i in range(2): f.readline() #useless lines
        Ek=[]
        nrows=int(self.nbands/8)+(1 if self.nbands%8!=0 else 0) #energies in block of 8 columns for each k 
        for iline in range(nrows):  
            Ek += self.__correctline(f.readline().split())
        f.readline()#blank line
        return Ek


    def __correctline(self,l):
        ret=[]
        for el in l:
            if len(el.split("-"))>2:
                for e in el.split("-")[1:]:
                    ret.append(-1.*float(e))
            else:
                ret.append(float(el))
        return ret
            
    def detect_hsp(self,K):
        'returns True if K is a HSP'
        for kpt in range(self.hsp.shape[0]):
            if np.allclose(K,self.hsp[kpt,:],rtol=0.0,atol=1e-6) == True: return True
        return False

    def _KPTMBJ_first(self,fKPT_MBJ="KPOINTS_MBJ_BS"):
        '''Determine the index of first kpt in band structure path. Used with MBJ.
        + Input:
            -fKPT_MBJ: kpt file of MBJ scf for band structure. kpts of path have weight 0.
        + Output:
            -kpt0: index of first kpt in band structure path.
        '''
        try:
            f=open(fKPT_MBJ,'r')
        except: #if file isnt found: if KPOINTS was generated with this program, this is recommendable
            print("Assuming that K-points of the path are placed first !")
            return 0
        for i in range(2):
            line=f.readline()
        nkpt=int(line)
        assert f.readline().split()[0].lower()!='r', 'KPOINTS in cartesian basis not implemented'
        for ikpt in range(nkpt):
            line=f.readline().split()
            if float(line[3])<1e-4: 
                return ikpt

class str2:
        
    def __init__(self,s):
        self.s=s

    def bool(self):
        if self.s[0]==".": return (True if self.s[1].lower()=="t" else False)
        else: return (True if self.s[0].lower()=="t" else False)


class Plotting():

    def __init__(self,BS):
        self.Ekn = BS.Ekn
        self.X = BS.X
        self.Xgrid = BS.Xgrid
        self.kpnames = BS.kpnames
        self.spinor = BS.spinor
        self.Nele = BS.Nele

    def plotbands(self, color=None, ybot=None , ytop=None, title=None, fsave=None):
        '''Generates the figure with the bands.
            +Input:
                -Ekn [Nk,IBstart-IBend+1]
                -X [Nk]
                -Xhsp: positions of HSPs
                -kpnames [list]: labels for HSPs.
                -ybot & ytop [float]
                '''
        fig=plt.figure(figsize= (10.,7.))
        ax = fig.add_subplot(1, 1, 1)
        ax = self.__place_bands(ax, color, ybot, ytop, title)
        if fsave is not None:
            plt.savefig(fsave, format='png')
        else:
            plt.show()

    def compare_wannier(self, Ekn_wannier, X_wannier, EF=0.0, color=None, ybot=None , ytop=None, title=None, fsave=None):
        '''
        Compare bands with W90 band structure
        '''
        fig,ax = plt.subplots(figsize= (10.,7.))
        ax = self.__place_bands(ax, color=color, ybot=ybot, ytop=ytop, title=title)
        for i_band in range(Ekn_wannier.shape[1]):
            ax.scatter(X_wannier, Ekn_wannier[:,i_band]-EF, marker='.', color='green', s=2.5)
        if fsave is None:
            plt.show()
        else:
            plt.savefig(fsave, format='png')

    def __place_bands(self, ax, color=None, ybot=None , ytop=None, title=None):
        '''Place bands and decorate axis with labels, titles,...'''
        # calculate Nocc (numb. valence bands)
        Nocc = None
        if color is None: # distinguish valence from conduction
            if self.spinor:
                self.Nocc = self.Nele
            else: #trivial spin degen
                self.Nocc = int(self.Nele/2) if self.Nele%2==0 else int((self.Nele+1)/2) 
        # set color and place bands
        for n in range(self.Ekn.shape[1]):
            if color is None : # color not given -> separate occ/unocc 
                if n+1 <= self.Nocc: c = "blue"
                else: c = "red"
            else:
                c = color
            ax.plot(self.X, self.Ekn[:,n], c=c)
        # decorate axes
        ax.set_ylabel(r'E-E$_{f}$ (eV)',fontsize= 15)
        if ybot is None:
            ybot= np.min(self.Ekn)-0.5
        if ytop is None:
            ytop= np.max(self.Ekn)+0.5
        ax.set_ylim(top= ytop, bottom= ybot)
        ax.set_xticks(self.Xgrid)
        if self.kpnames is not None: ax.set_xticklabels(self.kpnames)
        ax.margins(x= 0.)
        ax.grid(axis= 'x')
        ax.hlines(0., xmin= np.min(self.X), xmax= np.max(self.X), linewidth= 0.5)
        ax.tick_params(labelsize= 15)
        ax.tick_params(axis="y", which="major", labelsize= 15)
        # ax.tick_params(axis="y", which="minor", labelsize= 15)
        ax.set_title(title)
        return ax